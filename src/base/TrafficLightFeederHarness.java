/**
 * 
 */
package base;

/**
 * @author Vanessa_CU
 *
 */
public class TrafficLightFeederHarness {
	
	public static void main(String[] args) throws Exception{
		BaseInput input = new BaseInput();
		// construct a state machine
		StateMachine machine = new StateMachine();
		
		// loop through sending inputs to the state machine. 
		while (true) {
			machine.handle(input);
			System.out.println(machine.toString());
			
			Thread.sleep(1000);
		}
	}
}
