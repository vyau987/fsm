/**
 * 
 */
package base;

/**
 * @author Vanessa_CU
 * this is the super class for each specific state
 * that the overall system may be in 
 * at a particular point in time
 */
public abstract class BaseState {
	protected BaseState nextState;
	
	public abstract BaseState handle(BaseInput input);
	
	public abstract void init();
}
