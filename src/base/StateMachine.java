/**
 * 
 */
package base;

import states.Amber;
import states.Green;
import states.Red;
import states.RedAndAmber;

/**
 * @author Vanessa_CU
 *
 */
public class StateMachine {
	private BaseState currentState;
	public static BaseState RED = new Red();
	public static BaseState AMBER = new Amber();
	public static BaseState GREEN = new Green();
	public static BaseState RED_AND_AMBER = new RedAndAmber();
	
	
	public StateMachine() {
		// ensure to init all the states
		StateMachine.RED.init();
		StateMachine.AMBER.init();
		StateMachine.GREEN.init();
		StateMachine.RED_AND_AMBER.init();
		
		// Initialise the state machine to a default stating point
		this.currentState = StateMachine.RED;
	}
	
	public void handle(BaseInput input) {
		// synchronized ensures only one input can execute this block of code at once 
		// (i.e. if they want to work on this object at the same time
		synchronized(this) {
			// delegate input received to the currently active state
			this.currentState = currentState.handle(input);	
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "current state: " + this.currentState.toString();
	}
}
