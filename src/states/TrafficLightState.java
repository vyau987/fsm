/**
 * 
 */
package states;

import base.BaseInput;
import base.BaseState;

/**
 * @author Vanessa_CU
 *
 */
public class TrafficLightState extends BaseState {
	
	public BaseState handle(BaseInput input) {
		// nothing to be done with input
		// so simply transition to next state
		return this.nextState;
	}

	public void init(){};
	
}
