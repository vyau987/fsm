/**
 * 
 */
package states;

import base.BaseInput;
import base.BaseState;
import base.StateMachine;

/**
 * @author Vanessa_CU
 *
 */
public class Red extends TrafficLightState {

	public void init(){
		this.nextState = StateMachine.RED_AND_AMBER;
	}
	
}
